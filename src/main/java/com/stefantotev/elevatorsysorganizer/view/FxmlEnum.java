package com.stefantotev.elevatorsysorganizer.view;

/**
 * Created by relax4o on 19.2.2017 г..
 */
public enum FxmlEnum {

	MAIN {
		@Override
		public String getFxmlFile() {
			return "/fxml/main.fxml";
		}
	},
	DASHBOARD {
		@Override
		public String getFxmlFile() {
			return "/fxml/dashboard.fxml";
		}
	},
	ADD_ADDRESS {
		@Override
		public String getFxmlFile() {
			return "/fxml/add_address.fxml";
		}
	};

	public abstract String getFxmlFile();
}
