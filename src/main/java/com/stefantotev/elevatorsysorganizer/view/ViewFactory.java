package com.stefantotev.elevatorsysorganizer.view;

import com.stefantotev.elevatorsysorganizer.enums.ImageTypes;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Created by relax4o on 3.3.2017 г..
 */
public class ViewFactory {

	public static ImageView resolveImage(ImageTypes imageType) {

		ImageView image;

		switch( imageType ) {
			case IMAGE_AVAILABLE:
				image = new ImageView(new Image(ViewFactory.class.getResourceAsStream("/images/check_24.png")));
				break;
			case IMAGE_UNAVAILABLE:
				image = new ImageView(new Image(ViewFactory.class.getResourceAsStream("/images/remove_24.png")));
				break;
			default:
				image = null;
				break;
		}

		return image;
	}
}
