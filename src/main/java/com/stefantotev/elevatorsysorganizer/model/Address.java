package com.stefantotev.elevatorsysorganizer.model;

import javafx.beans.property.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by relax4o on 21.2.2017 г..
 */
public class Address implements BeanModel {

	private static Map<String, String> formattedAddresses = new HashMap<>();

	private final SimpleIntegerProperty id;

	private final SimpleStringProperty country;
	private final SimpleStringProperty city;

	private final String name;
	private final boolean isStreet;
	private final int number;
	private final int entrance;

	private final SimpleIntegerProperty floors;
	private final SimpleIntegerProperty totalElevators;

	private final SimpleBooleanProperty hasGroundFloor;
	private final SimpleBooleanProperty hasBasement;

	private final SimpleObjectProperty<Elevator> elevator;

	private final SimpleStringProperty fullAddressName;

	public static class Builder {
		// required
		private final int id;
		private final String name;
		private final Elevator elevator;

		// default
		private String country;
		private String city;
		private boolean isStreet = false;
		private int entrance = -1;
		private int floors = -1;
		private int totalElevators = -1;
		private int number = -1;
		private boolean hasGroundFloor = false;
		private boolean hasBasement = false;

		public Builder(int id, String name, Elevator elevator) {
			this.id = id;
			this.name = name;
			this.elevator = elevator;
		}

		public Builder setNumber(int number) {
			this.number = number;
			return this;
		}

		public Builder setCountry(String country) {
			this.country = country;
			return this;
		}

		public Builder setCity(String city) {
			this.city = city;
			return this;
		}

		public Builder setStreet(int street) {
			isStreet = (street == 1);
			return this;
		}

		public Builder setEntrance(int entrance) {
			this.entrance = entrance;
			return this;
		}

		public Builder setFloors(int floors) {
			this.floors = floors;
			return this;
		}

		public Builder setTotalElevators(int totalElevators) {
			this.totalElevators = totalElevators;
			return this;
		}

		public Builder setGroundFloor(int hasGroundFloor) {
			this.hasGroundFloor = (hasGroundFloor == 1);
			return this;
		}

		public Builder setBasement(int hasBasement) {
			this.hasBasement = (hasBasement == 1);
			return this;
		}

		public Address build() {
			return new Address(this);
		}
	}

	private Address(Builder builder) {
		this.id = new SimpleIntegerProperty(builder.id);
		this.country = new SimpleStringProperty(builder.country);
		this.city = new SimpleStringProperty(builder.city);
		this.name = builder.name;
		this.isStreet = builder.isStreet;
		this.number = builder.number;
		this.entrance = builder.entrance;
		this.floors = new SimpleIntegerProperty(builder.floors);
		this.totalElevators = new SimpleIntegerProperty(builder.totalElevators);
		this.hasGroundFloor = new SimpleBooleanProperty(builder.hasGroundFloor);
		this.hasBasement = new SimpleBooleanProperty(builder.hasBasement);
		this.fullAddressName = new SimpleStringProperty(fullAddressName());
		this.elevator = new SimpleObjectProperty<>(builder.elevator);

		formattedAddresses.put(fullAddressName(), this.name);
	}

	public int getId() {
		return id.get();
	}

	public String getCountry() {
		return country.get();
	}

	public String getCity() {
		return city.get();
	}

	public String getName() {
		return name;
	}

	public boolean isStreet() {
		return isStreet;
	}

	public int getNumber() {
		return number;
	}

	public int getEntrance() {
		return entrance;
	}

	public int getFloors() {
		return floors.get() == -1 ? 1 : floors.get();
	}

	public int getTotalElevators() {
		return totalElevators.get() == -1 ? 1 : totalElevators.get();
	}

	public boolean isHasGroundFloor() {
		return hasGroundFloor.get();
	}

	public boolean isHasBasement() {
		return hasBasement.get();
	}

	public String getFullAddressName() {
		return fullAddressName.get();
	}

	public SimpleIntegerProperty idProperty() {
		return id;
	}

	public SimpleStringProperty countryProperty() {
		return country;
	}

	public SimpleStringProperty cityProperty() {
		return city;
	}

	public SimpleIntegerProperty floorsProperty() {
		return floors;
	}

	public SimpleIntegerProperty totalElevatorsProperty() {
		return new SimpleIntegerProperty(getTotalElevators());
	}

	public SimpleBooleanProperty hasGroundFloorProperty() {
		return hasGroundFloor;
	}

	public SimpleBooleanProperty hasBasementProperty() {
		return hasBasement;
	}

	public SimpleObjectProperty<Elevator> elevatorProperty() {
		return elevator;
	}

	public SimpleStringProperty fullAddressNameProperty() {
		return fullAddressName;
	}

	public String fullAddressName() {
		StringBuilder sb = new StringBuilder();

		if ( number > 0 ) {
			if ( isStreet ) {
				sb.append("ул. ");
			} else {
				sb.append("ж.к. ");
			}
		}
		sb.append(name);
		if ( number > 0 ) {
			sb.append(" ");
			if ( isStreet ) {
				sb.append("№");
			} else {
				sb.append("бл.");
			}
			sb.append(number);
			if ( !isStreet ) {
				sb.append(" ");
				sb.append("вх.");
				sb.append(entrance);
			}
		}

		return sb.toString();
	}

	public static Map<String, String> getFormattedAddresses() {
		return formattedAddresses;
	}

	public Elevator getElevator() {
		return elevator.get();
	}
}
