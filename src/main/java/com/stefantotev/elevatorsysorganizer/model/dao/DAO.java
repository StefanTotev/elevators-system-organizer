package com.stefantotev.elevatorsysorganizer.model.dao;

import com.stefantotev.elevatorsysorganizer.model.ConnectionFactory;
import com.stefantotev.elevatorsysorganizer.model.extractor.Extractor;

import java.sql.Connection;

/**
 * Created by relax4o on 4.3.2017 г..
 */
public abstract class DAO {
	protected Connection connection = ConnectionFactory.getInstance().getConnection();
	protected Extractor extractor;
}
