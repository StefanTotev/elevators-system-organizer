package com.stefantotev.elevatorsysorganizer.model.dao;

import com.stefantotev.elevatorsysorganizer.model.Address;

import java.util.List;

/**
 * Created by relax4o on 28.2.2017 г..
 */
public interface AddressDAO {
	List<Address> retrieveAllAddressRecords();
	Address getAddress(int id);
	void insertAddress(Address address);
	void updateAddress(Address address);
	void deleteAddress(Address address);
}
