package com.stefantotev.elevatorsysorganizer.model.dao;

import com.stefantotev.elevatorsysorganizer.model.Address;
import com.stefantotev.elevatorsysorganizer.model.extractor.AddressExtractor;
import com.stefantotev.elevatorsysorganizer.model.extractor.Extractor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by relax4o on 28.2.2017 г..
 */
public class AddressDAOImpl extends DAO implements AddressDAO {

	public AddressDAOImpl() {
		this.extractor = new Extractor(new AddressExtractor());
	}

	@Override
	public List<Address> retrieveAllAddressRecords() {
		String query = "SELECT * FROM all_addresses";

		try(Statement stmt = connection.createStatement()) {
			ResultSet rs = stmt.executeQuery(query);

			List<Address> addresses = new ArrayList<>();
			while( rs.next() ) {
				Address address = (Address)extractor.extractFromResultSet(rs);
				addresses.add(address);
			}

			return addresses;
		} catch ( SQLException e ) {
			System.out.println("Query failed: " + e.getMessage());
			return null;
		}
	}

	@Override
	public Address getAddress(int id) {
		String query = "SELECT * FROM all_addresses WHERE address_id = ?";

		try(PreparedStatement stmt = connection.prepareStatement(query)) {
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if ( rs.next() ) {
				return (Address)extractor.extractFromResultSet(rs);
			}
		} catch ( SQLException e ) {
			System.out.println("Query failed: " + e.getMessage());
		}

		return null;
	}

	@Override
	public void insertAddress(Address address) {

	}

	@Override
	public void updateAddress(Address address) {

	}

	@Override
	public void deleteAddress(Address address) {

	}
}
