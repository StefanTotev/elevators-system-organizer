package com.stefantotev.elevatorsysorganizer.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by relax4o on 24.2.2017 г..
 */
public class ConnectionFactory {
	private static final String databaseFileLocation = ConnectionFactory.class.getResource("/elevatorsystem.db").toExternalForm();
    private static final String CONNECTION_URI = "jdbc:sqlite:" + databaseFileLocation;

	private Connection conn;
	private static ConnectionFactory instance = new ConnectionFactory();

	private ConnectionFactory() {
    }

    public static ConnectionFactory getInstance() {
		return instance;
	}

	public void open() {
        try {
		    System.out.println("Database connection is opening...");
			conn = DriverManager.getConnection(CONNECTION_URI);
		} catch ( SQLException e ) {
			System.out.println("Couldn't connect to database: " + e.getMessage());
		}
	}

	public void close() {
		try {
			if ( conn != null ) {
				System.out.println("Database connection is closing...");
				conn.close();
			}
		} catch ( SQLException e ) {
			System.out.println("Couldn't close connection: " + e.getMessage());
		}
	}

	public Connection getConnection() {
		return conn;
	}
}
