package com.stefantotev.elevatorsysorganizer.model;

import com.stefantotev.elevatorsysorganizer.enums.ElevatorPartsTypes;
import javafx.beans.property.*;

/**
 * Created by relax4o on 22.2.2017 г..
 */
public class Elevator implements BeanModel {
	private final SimpleIntegerProperty id;
	private final SimpleStringProperty type;
	private final SimpleStringProperty controlPanel;
	private final SimpleStringProperty controller;
	private final SimpleStringProperty manufacturer;
	private final SimpleObjectProperty<Scheme> schemeType;
	private final SimpleStringProperty doors;
	private final SimpleIntegerProperty maxWeight;
	private final SimpleIntegerProperty capacity;
	private final SimpleDoubleProperty velocity;

	public static class Builder {
		// required
		private final int id;
		private final String type;

		//default
		private String controlPanel;
		private String controller;
		private String manufacturer;
		private Scheme schemeType;
		private String doors;
		private int maxWeight = -1;
		private int capacity = -1;
		private double velocity = -1;

		public Builder(int id, String type) {
			this.id = id;
			this.type = type;
		}

		public Builder setControlPanel(String controlPanel) {
			this.controlPanel = controlPanel;
			return this;
		}

		public Builder setController(String controller) {
			this.controller = controller;
			return this;
		}

		public Builder setManufacturer(String manufacturer) {
			this.manufacturer = manufacturer;
			return this;
		}

		public Builder setSchemeType(Scheme schemeType) {
			this.schemeType = schemeType;
			return this;
		}

		public Builder setDoors(int doors) {
			switch(doors) {
				case 1:
					this.doors = ElevatorPartsTypes.DoorTypes.SEMIAUTOMATIC.getType();
					break;
				case 2:
					this.doors = ElevatorPartsTypes.DoorTypes.AUTOMATIC.getType();
					break;
				case 3:
					this.doors = ElevatorPartsTypes.DoorTypes.BOTH.getType();
					break;
				default:
					this.doors = ElevatorPartsTypes.DoorTypes.SEMIAUTOMATIC.getType();
					break;
			}

			return this;
		}

		public Builder setMaxWeight(int maxWeight) {
			this.maxWeight = maxWeight;
			return this;
		}

		public Builder setCapacity(int capacity) {
			this.capacity = capacity;
			return this;
		}

		public Builder setVelocity(double velocity) {
			this.velocity = velocity;
			return this;
		}

		public Elevator build() {
			return new Elevator(this);
		}
	}

	private Elevator(Builder builder) {
		this.id = new SimpleIntegerProperty(builder.id);
		this.type = new SimpleStringProperty(builder.type);
		this.controlPanel = new SimpleStringProperty(builder.controlPanel);
		this.controller = new SimpleStringProperty(builder.controller);
		this.manufacturer = new SimpleStringProperty(builder.manufacturer);
		this.schemeType = new SimpleObjectProperty<>(builder.schemeType);
		this.doors = new SimpleStringProperty(builder.doors);
		this.maxWeight = new SimpleIntegerProperty(builder.maxWeight);
		this.capacity = new SimpleIntegerProperty(builder.capacity);
		this.velocity = new SimpleDoubleProperty(builder.velocity);
	}

	public Integer getId() {
		return id.get();
	}

	public SimpleIntegerProperty idProperty() {
		return this.id;
	}

	public String getType() {
		return type.get();
	}

	public SimpleStringProperty typeProperty() {
		return type;
	}

	public String getControlPanel() {
		return controlPanel.get();
	}

	public SimpleStringProperty controlPanelProperty() {
		return controlPanel;
	}

	public String getController() {
		return controller.get();
	}

	public SimpleStringProperty controllerProperty() {
		return controller;
	}

	public String getManufacturer() {
		return manufacturer.get();
	}

	public SimpleStringProperty manufacturerProperty() {
		return manufacturer;
	}

	public Scheme getSchemeType() {
		return schemeType.get();
	}

	public SimpleObjectProperty<Scheme> schemeTypeProperty() {
		return schemeType;
	}

	public String getDoors() {
		return doors.get();
	}

	public SimpleStringProperty doorsProperty() {
		return doors;
	}

	public int getMaxWeight() {
		return maxWeight.get();
	}

	public SimpleIntegerProperty maxWeightProperty() {
		return maxWeight;
	}

	public int getCapacity() {
		return capacity.get();
	}

	public SimpleIntegerProperty capacityProperty() {
		return capacity;
	}

	public double getVelocity() {
		return velocity.get();
	}

	public SimpleDoubleProperty velocityProperty() {
		return velocity;
	}
}
