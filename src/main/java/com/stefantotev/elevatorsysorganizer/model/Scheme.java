package com.stefantotev.elevatorsysorganizer.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by relax4o on 28.2.2017 г..
 */
public class Scheme implements BeanModel {
	private int id;
	private SimpleStringProperty name;
	private SimpleStringProperty description;

	public Scheme(int id, String name, String description) {
		this.id = id;
		this.name = new SimpleStringProperty(name);
		this.description = new SimpleStringProperty(description);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name.get();
	}

	public SimpleStringProperty nameProperty() {
		return name;
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public String getDescription() {
		return description.get();
	}

	public SimpleStringProperty descriptionProperty() {
		return description;
	}

	public void setDescription(String description) {
		this.description.set(description);
	}
}
