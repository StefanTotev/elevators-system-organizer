package com.stefantotev.elevatorsysorganizer.model.extractor;

import com.stefantotev.elevatorsysorganizer.model.Scheme;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by relax4o on 4.3.2017 г..
 */
public class SchemeExtractor implements Extractable {

	@Override
	public Scheme extractFromResultSet(ResultSet rs) throws SQLException {
		Scheme scheme = new Scheme(rs.getInt("scheme_id"),
				rs.getString("scheme_name"),
				rs.getString("description"));

		return scheme;
	}
}
