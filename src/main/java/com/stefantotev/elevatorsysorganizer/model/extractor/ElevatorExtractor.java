package com.stefantotev.elevatorsysorganizer.model.extractor;

import com.stefantotev.elevatorsysorganizer.model.Elevator;
import com.stefantotev.elevatorsysorganizer.model.Scheme;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by relax4o on 4.3.2017 г..
 */
public class ElevatorExtractor implements Extractable {

	public ElevatorExtractor() {
	}

	@Override
	public Elevator extractFromResultSet(ResultSet rs) throws SQLException {
		Elevator elevator = new Elevator.Builder(rs.getInt("elevator_id"), rs.getString("type"))
				.setManufacturer(rs.getString("manufacturer"))
				.setCapacity(rs.getInt("capacity"))
				.setDoors(rs.getInt("doors"))
				.setMaxWeight(rs.getInt("max_weight"))
				.setVelocity(rs.getDouble("velocity"))
				.setSchemeType((Scheme)new Extractor(new SchemeExtractor()).extractFromResultSet(rs))
				.build();

		return elevator;
	}
}
