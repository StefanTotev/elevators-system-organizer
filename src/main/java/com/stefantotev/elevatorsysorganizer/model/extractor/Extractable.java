package com.stefantotev.elevatorsysorganizer.model.extractor;

import com.stefantotev.elevatorsysorganizer.model.BeanModel;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by relax4o on 4.3.2017 г..
 */
public interface Extractable {
	BeanModel extractFromResultSet(ResultSet rs) throws SQLException;
}
