package com.stefantotev.elevatorsysorganizer.model.extractor;

import com.stefantotev.elevatorsysorganizer.model.Address;
import com.stefantotev.elevatorsysorganizer.model.Elevator;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by relax4o on 4.3.2017 г..
 */
public class AddressExtractor implements Extractable {

	public AddressExtractor() {
	}

	@Override
	public Address extractFromResultSet(ResultSet rs) throws SQLException {
		Address address = new Address.Builder(rs.getInt("address_id"), rs.getString("address"), (Elevator)new Extractor(new ElevatorExtractor()).extractFromResultSet(rs))
				.setBasement(rs.getInt("basement"))
				.setCity(rs.getString("city"))
				.setCountry(rs.getString("country"))
				.setEntrance(rs.getInt("entrance"))
				.setFloors(rs.getInt("floors"))
				.setGroundFloor(rs.getInt("g_floor"))
				.setNumber(rs.getInt("number"))
				.setStreet(rs.getInt("is_street"))
				.build();

		return address;
	}
}
