package com.stefantotev.elevatorsysorganizer.controller.tasks;

import com.stefantotev.elevatorsysorganizer.model.Address;
import com.stefantotev.elevatorsysorganizer.model.dao.AddressDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.util.List;

/**
 * Created by relax4o on 2.3.2017 г..
 */
public class AddressDataRetrieverService extends Service<ObservableList<Address>> {

	AddressDAO addressDAO;

	public AddressDataRetrieverService(AddressDAO addressDAO) {
		this.addressDAO = addressDAO;
	}

	@Override
	protected Task<ObservableList<Address>> createTask() {
		return new Task<ObservableList<Address>>() {
			@Override
			protected ObservableList<Address> call() throws Exception {
				List<Address> addressList = addressDAO.retrieveAllAddressRecords();
				int listLength = addressList.size();

				int i = 0;
				while ( listLength > i && !isDone() ) {
					if ( isCancelled() ) {
						return null;
					}

					// For simulating loading time
					try {
						Thread.sleep(80);
					} catch ( InterruptedException e) {

					}

					i++;
//					updateProgress(++i, listLength);
				}

				return FXCollections.observableArrayList(addressList);
			}
		};
	}
}
