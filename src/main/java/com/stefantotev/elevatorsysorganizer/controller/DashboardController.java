package com.stefantotev.elevatorsysorganizer.controller;

import com.stefantotev.elevatorsysorganizer.controller.tasks.AddressDataRetrieverService;
import com.stefantotev.elevatorsysorganizer.enums.ImageTypes;
import com.stefantotev.elevatorsysorganizer.model.Address;
import com.stefantotev.elevatorsysorganizer.model.dao.AddressDAO;
import com.stefantotev.elevatorsysorganizer.model.dao.AddressDAOImpl;
import com.stefantotev.elevatorsysorganizer.view.ViewFactory;
import javafx.concurrent.Service;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import java.util.Comparator;

/**
 * Created by relax4o on 12.2.2017 г..
 */
public class DashboardController {

	private AddressDAO addressDAO = new AddressDAOImpl();

	@FXML private AnchorPane dashboardAnchorPane;
	@FXML private BorderPane addressListPane;

	@FXML private TableView<Address> addressTableView;

	@FXML private TableColumn<Address, Number> addressId;
	@FXML private TableColumn<Address, Number> docElevatorId;
	@FXML private TableColumn<Address, String> addressCountry;
	@FXML private TableColumn<Address, String> addressCity;
	@FXML private TableColumn<Address, String> addressName;
	@FXML private TableColumn<Address, Number> addressFloors;
	@FXML private TableColumn<Address, Number> addressTotalElevators;
	@FXML private TableColumn<Address, Boolean> addressGroundFloor;
	@FXML private TableColumn<Address, Boolean> addressBasement;

	@FXML private TableColumn<Address, String> elevatorType;
	@FXML private TableColumn<Address, String> elevatorManufacturer;
	@FXML private TableColumn<Address, Number> elevatorMaxWeight;
	@FXML private TableColumn<Address, Number> elevatorMaxCapacity;
	@FXML private TableColumn<Address, Number> elevatorVelocity;
	@FXML private TableColumn<Address, String> elevatorSchemeType;
	@FXML private TableColumn<Address, String> elevatorDoors;

	@FXML private ToolBar addressListToolBar;
	@FXML private TextField searchTextField;
	@FXML private ChoiceBox<String> filterChoiceBox;
	@FXML private Button chooseButton;
	@FXML private Button deleteButton;
	@FXML private Button reloadButton;
	private ProgressIndicator progressIndicator;

	private Service service;

	@FXML
	public void initialize() {

		service = new AddressDataRetrieverService(addressDAO);
		progressIndicator = new ProgressIndicator();
		progressIndicator.setMaxSize(70, 70);

		addressTableView.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);

		registerCellValueFactories();
		registerCellFactories();
		registerCellComparators();

		loadAddressData();


	}

	@FXML
	public void reloadAddressData() {
		loadAddressData();
	}

	private void loadAddressData() {

		searchTextField.disableProperty().bind(service.stateProperty().isEqualTo(Worker.State.RUNNING));
		filterChoiceBox.disableProperty().bind(service.stateProperty().isEqualTo(Worker.State.RUNNING));
		chooseButton.disableProperty().bind(service.stateProperty().isEqualTo(Worker.State.RUNNING));
		reloadButton.disableProperty().bind(service.stateProperty().isEqualTo(Worker.State.RUNNING));
		deleteButton.disableProperty().bind(service.stateProperty().isEqualTo(Worker.State.RUNNING));

		progressIndicator.progressProperty().bind(service.progressProperty());
		addressTableView.itemsProperty().bind(service.valueProperty());
		addressTableView.setPlaceholder(progressIndicator);

		service.restart();
	}

	private void registerCellValueFactories() {
		addressId.setCellValueFactory(cellData -> cellData.getValue().idProperty());
		docElevatorId.setCellValueFactory(cellData -> cellData.getValue().getElevator().idProperty());
		addressCountry.setCellValueFactory(cellData -> cellData.getValue().countryProperty());
		addressCity.setCellValueFactory(cellData -> cellData.getValue().cityProperty());
		addressName.setCellValueFactory(cellData -> cellData.getValue().fullAddressNameProperty());
		addressFloors.setCellValueFactory(cellData -> cellData.getValue().floorsProperty());
		addressTotalElevators.setCellValueFactory(cellData -> cellData.getValue().totalElevatorsProperty());
		addressGroundFloor.setCellValueFactory(cellData -> cellData.getValue().hasGroundFloorProperty());
		addressBasement.setCellValueFactory(cellData -> cellData.getValue().hasBasementProperty());

		elevatorType.setCellValueFactory(cellData -> cellData.getValue().getElevator().typeProperty());
		elevatorManufacturer.setCellValueFactory(cellData -> cellData.getValue().getElevator().manufacturerProperty());
		elevatorMaxWeight.setCellValueFactory(cellData -> cellData.getValue().getElevator().maxWeightProperty());
		elevatorMaxCapacity.setCellValueFactory(cellData -> cellData.getValue().getElevator().capacityProperty());
		elevatorVelocity.setCellValueFactory(cellData -> cellData.getValue().getElevator().velocityProperty());
		elevatorSchemeType.setCellValueFactory(cellData -> cellData.getValue().getElevator().getSchemeType().nameProperty());
		elevatorDoors.setCellValueFactory(cellData -> cellData.getValue().getElevator().doorsProperty());
	}

	private void registerCellFactories() {

		addressGroundFloor.setCellFactory(new Callback<TableColumn<Address, Boolean>, TableCell<Address, Boolean>>() {
			@Override
			public TableCell<Address, Boolean> call(TableColumn<Address, Boolean> param) {
				TableCell<Address, Boolean> groundFloorCell = new TableCell<Address, Boolean>() {
					@Override
					protected void updateItem(Boolean item, boolean empty) {
						ImageView image;
						if ( item != null ) {
							if ( item ) {
								image = ViewFactory.resolveImage(ImageTypes.IMAGE_AVAILABLE);
							} else {
								image = ViewFactory.resolveImage(ImageTypes.IMAGE_UNAVAILABLE);
							}

							image.setFitHeight(13);
							image.setFitWidth(13);
							setGraphic(image);
						}
						else {
							setGraphic(null);
						}
					}
				};
				return groundFloorCell;
			}
		});

		addressBasement.setCellFactory(new Callback<TableColumn<Address, Boolean>, TableCell<Address, Boolean>>() {
			@Override
			public TableCell<Address, Boolean> call(TableColumn<Address, Boolean> param) {
				TableCell<Address, Boolean> basementCell = new TableCell<Address, Boolean>() {
					@Override
					protected void updateItem(Boolean item, boolean empty) {
						ImageView image;
						if ( item != null ) {
							if ( item ) {
								image = ViewFactory.resolveImage(ImageTypes.IMAGE_AVAILABLE);
							} else {
								image = ViewFactory.resolveImage(ImageTypes.IMAGE_UNAVAILABLE);
							}

							image.setFitHeight(13);
							image.setFitWidth(13);
							setGraphic(image);
						}
						else {
							setGraphic(null);
						}
					}
				};
				return basementCell;
			}
		});
	}

	private void registerCellComparators() {
		// Force address column to be sorted through single "name" property
		addressName.setComparator(new Comparator<String>() {
			String address1, address2;

			@Override
			public int compare(String o1, String o2) {
				address1 = Address.getFormattedAddresses().get(o1);
				address2 = Address.getFormattedAddresses().get(o2);
				return address1.compareTo(address2);
			}
		});

		docElevatorId.setComparator(new Comparator<Number>() {
			Integer id1, id2;

			@Override
			public int compare(Number o1, Number o2) {
				id1 = (Integer) o1;
				id2 = (Integer) o2;

				return id1.compareTo(id2);
			}
		});
	}
}
