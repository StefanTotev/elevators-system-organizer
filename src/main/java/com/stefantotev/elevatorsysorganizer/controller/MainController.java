package com.stefantotev.elevatorsysorganizer.controller;

import com.stefantotev.elevatorsysorganizer.view.FxmlEnum;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainController {

	@FXML private BorderPane rootLayout;
	@FXML private ToggleGroup mainMenuToggleGroup;
	private Parent currentLayout;

	private final List<FxmlEnum> menuLayoutLocations = new ArrayList<FxmlEnum>(Arrays.asList(
			FxmlEnum.DASHBOARD,
			FxmlEnum.ADD_ADDRESS
	));

	/**
	 * TODO: Start building ConnectionFactory classes ConnectionFactory, Elevators, Addresses etc...
	 */

	@FXML
	public void initialize() {

		// Loading selected menu's layout and prevents
		// from selecting the same menu button
		mainMenuToggleGroup.selectedToggleProperty().addListener( (obsVal, oldT, newT) -> {

			// When new target is selected we have to ensure
			// that the old one is enabled for selecting
			if ( oldT != null ) {
				((ToggleButton) oldT).setDisable(false);
			}

//			rootLayout.setCenter(null);
			try {
				ToggleButton selectedToggle = (ToggleButton) newT;
				selectedToggle.setDisable(true);

				if ( menuLayoutLocations.size() > 0
						&& menuLayoutLocations.size() > mainMenuToggleGroup.getToggles().indexOf(newT) ) {
					currentLayout = FXMLLoader.load(getClass().getResource(menuLayoutLocations.get(mainMenuToggleGroup.getToggles().indexOf(newT)).getFxmlFile()));
					rootLayout.setCenter(currentLayout);
				}
			} catch ( Exception e ) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		});

		// Selecting first button by default
		mainMenuToggleGroup.getToggles().get(0).setSelected(true);
	}
}
