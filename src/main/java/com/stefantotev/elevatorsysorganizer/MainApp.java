package com.stefantotev.elevatorsysorganizer;

import com.stefantotev.elevatorsysorganizer.model.ConnectionFactory;
import com.stefantotev.elevatorsysorganizer.view.FxmlEnum;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

public class MainApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    long startTime;
    long endTime;

    @Override
    public void init() throws Exception {
        startTime = System.currentTimeMillis();
        super.init();
        System.out.println("Application initializing...");
        ConnectionFactory.getInstance().open();
        System.out.println("Database connection opened!");
        System.out.println("Application initialized!");
    }

    @Override
    public void stop() throws Exception {
        ConnectionFactory.getInstance().close();
        System.out.println("Database connection closed!");
        super.stop();
    }

    @Override
    public void start(Stage primaryStage) {
        System.out.println("Application.start() initialize...");
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Някаква програма пък");
        this.primaryStage.getIcons().add(new Image("/images/elevator_arrows_32.png"));

        try {
            rootLayout = FXMLLoader.load(getClass().getResource(FxmlEnum.MAIN.getFxmlFile()));
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        Scene scene = new Scene(rootLayout);
        scene.setRoot(rootLayout);
        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());

        primaryStage.setScene(scene);
        primaryStage.setMaximized(false);
        primaryStage.setResizable(true);

        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

        primaryStage.setWidth(primaryScreenBounds.getWidth()/1.5);
        primaryStage.setHeight(primaryScreenBounds.getHeight()/1.5);

        primaryStage.show();

        endTime = System.currentTimeMillis();
        System.out.println("Application.start() initialized!");
        System.out.println("Time taken: " + (endTime - startTime) + "ms");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
