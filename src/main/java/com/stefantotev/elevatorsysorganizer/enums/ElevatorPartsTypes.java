package com.stefantotev.elevatorsysorganizer.enums;

/**
 * Created by relax4o on 24.2.2017 г..
 */
public class ElevatorPartsTypes {

	public enum DoorTypes {
		SEMIAUTOMATIC("ПАВ"), AUTOMATIC("АВ"), BOTH("ПАВ + АВ");

		String type;

		DoorTypes(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
	}

	public enum ControlPanelTypes {
		MICROSWITCHER, RELAY, CONTROLLER
	}

	public enum ElevatorTypes {
		ELECTRICAL, HYDRAULIC
	}
}
